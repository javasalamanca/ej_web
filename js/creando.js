//declaramos array de objetos, estructura de datos
var listaMotos = [
    // {url: "img/moto01.jpg", nota: "primera moto"},
    {
        url: "img/moto02.jpg",
        nota: "segunda moto"
    },
    {
        url: "img/moto03.jpg",
        nota: "tercera moto"
    },
    {
        url: "img/moto04.jpg",
        nota: "cuarta moto"
    },
];

    //declaramos array de objetos, estructura de datos
    var listaTelefonos = [{
        nombre: "ana",
        tel: "android"
    },
    {
        nombre: "javi",
        tel: "iphone"
    },
    {
        nombre: "carmen",
        tel: "android"
    },
    {
        nombre: "rubén",
        tel: "android"
    },
];




$(document).on("click", "button#crea1", function () {
    //añadimos "algo" al div marcador2
    $("#marcador1").append("<h2>Algo...</h1>");
});


$(document).on("click", "button#crea3", function () {

    //guardamos la sección tbody de la tabla dentro del div marcador3 en una variable, para uso posterior
    var tbodyTabla = $("div#marcador3 table tbody");


    //por cada elemento en el array listaTelefonos, añadimos una fila al tbody de la tabla
    listaTelefonos.forEach(function (elemento) {
        tbodyTabla.append("<tr><td>" + elemento.nombre + "</td><td>" + elemento.tel + "</td></tr>");
    });

});


$(document).on("click", "button#crea2", function () {

    //lista de strings que añadiremos a <ul>
    var lista = ["Segundo", "Tercero", "Cuarto", "Quinto", "Sexto"];

    //dos formas equivalentes de recorrer un array

    //con jQuery $().each()
    
    $(lista).each((clave, valor) => {
        $("#marcador2").append("<li>" + valor + "</li>")
    });
    
    //sin jQuery list.forEach() ***sí se utiliza jquery en append***
    /*
    lista.forEach(function (valor, clave) {
        $("#marcador2").append("<li>" + valor + "(" + clave + ")</li>");
    });
    */

});


$(document).on("click", "button#crea4", function () {
    //guardamos la sección tbody de la tabla dentro del div marcador3 en una variable, para uso posterior
    var sliderEnlaces = $("#sliderEnlaces");
    var sliderMotos = $("#sliderMotos");
    sliderEnlaces.empty();
    sliderMotos.empty();



    //por cada elemento en el array listaTelefonos, añadimos una fila al tbody de la tabla
    var contador = 0;
    listaMotos.forEach(function (elemento) {

        sliderEnlaces.append('<li data-target="#sliderMotos" data-slide-to="' +
            contador +
            '" ' +
            ((contador == 0) ? 'class="active"' : '') +
            '></li>')

        sliderMotos.append('<div class="carousel-item ' + ((contador == 0) ? 'active' : '') + '">' +
            '<img class="d-block w-100" src="' + elemento.url + '" alt="' + elemento.nota + '"></div>');

        contador++;
    });

    $('.carousel').carousel('cycle');
});



$(document).on("click", "button#crea5", function () {
    var sliderEnlaces = $("#divFotos");

    var foto1 = document.createElement("img");
    foto1.src="img/moto01.jpg";
    foto1.class="destacada";
    //document.getElementById("divFotos").appendChild(foto1);
    $("#divFotos").append(x);

    var foto2 = $("<img />").attr("src","img/moto02.jpg" ).addClass("destacada");
    $("#divFotos").append(foto2);

});



$(document).on("click", "button#ajax1", function () {
    $.getJSON("ajax/clientes.json", function (data) {

        var linea, iphones = 0,
            androides = 0,
            otros = 0,
            cuantos = 0,
            tpcAndroides, tpcIphones, tpcOtros;
        data.clientes.forEach(function (cliente) {
            linea = '<tr>' + '<td>' + cliente.id + '</td>' + '<td>' + cliente.nombre + '</td>' + '<td>' + cliente.tel + '</td>' + '</tr>';

            switch (cliente.tel) {
                case 'android':
                    androides++;
                    break;
                case 'iphone':
                    iphones++;
                    break;
                default:
                    otros++;
                    break;
            }

            $('#tabla-clientes tbody').append(linea);
            cuantos++;
        });

        tpcAndroides = 100 * androides / cuantos;
        tpcIphones = 100 * iphones / cuantos;
        tpcOtros = 100 * otros / cuantos;
        $("#tabla-clientes tfoot").append('<tr><td></td><td>Androides</td><td>' + tpcAndroides.toFixed(2) + " %</td></tr>");
        $("#tabla-clientes tfoot").append('<tr><td></td><td>Iphones</td><td>' + tpcIphones.toFixed(2) + " %</td></tr>");
        $("#tabla-clientes tfoot").append('<tr><td></td><td>Otros</td><td>' + tpcOtros.toFixed(2) + " %</td></tr>");


    });
});


$(document).on("click", "button#ajax2", function () {
    var id = $("#idInput").val();
    var encontrado = false;
    if (id == null || isNaN(id)) {
        $("#idInput").addClass("error");
        $("#idInput").val("");
    } else {

        $("#idInput").removeClass("error");
        $.getJSON("ajax/clientes.json", function (data) {
            data.clientes.forEach(function (cliente) {
                if (cliente.id == id) {
                    $('#tabla-detalle tbody').empty();
                    encontrado = true;
                    $('#tabla-detalle tbody').append('<tr><th>Id</th><td>' + cliente.id + '</td></tr>');
                    $('#tabla-detalle tbody').append('<tr><th>Nombre</th><td>' + cliente.nombre + '</td></tr>');
                    $('#tabla-detalle tbody').append('<tr><th>Tel</th><td>' + cliente.tel + '</td></tr>');
                    $('#tabla-detalle tbody').append('<tr><th>Email</th><td>' + cliente.email + '</td></tr>');

                }

            });

            if (encontrado == false) {
                $("#idInput").addClass("error");
                $("#idInput").val("");
            }

        });
    }

});