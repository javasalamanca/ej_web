
$(document).on("click", "button#ponVerde", function(){
	$(".row.bolas div div").css("background-color", "green");
});

$(document).on("click", "button#ponRojo", function(){
	$(".row.bolas div div").css("background-color", "red");
});


$(document).on("click", "button#ajax1", function(){
    $.getJSON( "ajax/clientes.json", function( data ) {
       
        var linea, iphones=0, androides=0, otros=0, cuantos=0, tpcAndroides, tpcIphones, tpcOtros;
        data.clientes.forEach(function(cliente)  {
            linea = '<tr>'+'<td>'+cliente.id+'</td>'+'<td>'+cliente.nombre+'</td>'+'<td>'+cliente.tel+'</td>'+'</tr>';
            
            switch(cliente.tel){
                case 'android': 
                    androides++;
                    break;
                case 'iphone': 
                    iphones++;
                    break;
                default:
                    otros++;
                    break;
            }
            
            $('#tabla-clientes tbody').append(linea);
            cuantos++;
        });

        tpcAndroides = 100*androides/cuantos;
        tpcIphones = 100*iphones/cuantos;
        tpcOtros = 100*otros/cuantos;
        $("#tabla-clientes tfoot").append('<tr><td></td><td>Androides</td><td>'+tpcAndroides.toFixed(2)+" %</td></tr>");
        $("#tabla-clientes tfoot").append('<tr><td></td><td>Iphones</td><td>'+tpcIphones.toFixed(2)+" %</td></tr>");
        $("#tabla-clientes tfoot").append('<tr><td></td><td>Otros</td><td>'+tpcOtros.toFixed(2)+" %</td></tr>");
      
        
      }); 
    });


      $(document).on("click", "button#ajax2", function(){
          var id = $("#idInput").val();
          var encontrado=false;
          if (id==null || isNaN(id)) {
              $("#idInput").addClass("error");
              $("#idInput").val("");
          }else{
            
            $("#idInput").removeClass("error");
            $.getJSON( "ajax/clientes.json", function( data ) {
                data.clientes.forEach(function(cliente)  {
                    if (cliente.id==id) {
                        $('#tabla-detalle tbody').empty();
                        encontrado=true;
                        $('#tabla-detalle tbody').append('<tr><th>Id</th><td>'+cliente.id+'</td></tr>');
                        $('#tabla-detalle tbody').append('<tr><th>Nombre</th><td>'+cliente.nombre+'</td></tr>');
                        $('#tabla-detalle tbody').append('<tr><th>Tel</th><td>'+cliente.tel+'</td></tr>');
                        $('#tabla-detalle tbody').append('<tr><th>Email</th><td>'+cliente.email+'</td></tr>');
     
                    }
          
            });

            if (encontrado==false){
                $("#idInput").addClass("error");
                $("#idInput").val("");
            }
        
        });
    }
 
});






